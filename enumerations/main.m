//
//  main.m
//  enumerations
//
//  Created by Fernando Rodríguez Romero on 06/04/15.
//  Copyright (c) 2015 Agbo. All rights reserved.
//

#import <Foundation/Foundation.h>

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        
       // NSString
        __block int idx = 0;
        NSMutableString *test = [@"Prueba" mutableCopy];
        [@"Hola\nMundo" enumerateLinesUsingBlock:^(NSString *line, BOOL *stop) {
            
            [test appendString:@"otro texto"];
            NSLog(@"Linea %d: %@", ++idx, line);
        }  ];
        
        
        
      // NSArray
        NSArray *episodes = @[@"IV", @"V", @"VI", @"I", @"II", @"III"];
        
        [episodes enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            
            
            NSLog(@"Episode %lu: %@",(unsigned long) idx, obj);
            
        }];
        
        
        [episodes enumerateObjectsWithOptions:NSEnumerationConcurrent
                                   usingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                                       
                                       
                                       NSLog(@"Episode %lu: %@",(unsigned long) idx, obj);
                                       
                                   }];
        
        // NSDictionary
        NSDictionary *bso = @{@"Star Wars" : @"Williams",
                              @"Inception" : @"Zimmer"};
        
        
        [bso enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
            
            NSLog(@"La bso de %@ ha sido compuesta por %@", key, obj);
        }];
        
        
    }
    return 0;
}
